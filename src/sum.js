function isIntegerString(a)
{
  if (a == null || a == undefined)
  {
    throw 'EBADOP';
  }

  return !!a && !a.match(/^\d+$/);
}

function integerOrEmptyString(x)
{
  if (isIntegerString(x))
  {
    x = ''
  }

  return x;

}

function setZeroIfEmptyString (x)
{
  x = integerOrEmptyString(x);
  return x === '' ?  0 : parseInt(x);
}
function sum(a, b)
{
  
  a = setZeroIfEmptyString(a)
  b = setZeroIfEmptyString(b)

  return a + b;

}

module.exports = sum;