const sum = require('../src/sum');

test('both operands are empty strings', () => {
  expect(sum('', '')).toBe(0);
});

test('first operand is string, other is 1', () => {
  expect(sum('1', '')).toBe(1);
});

test('second operand is string, other is 1', () => {
  expect(sum('', '1')).toBe(1);
});

test('operands are both integers, 1+1=2', () => {
  expect(sum('1', '1')).toBe(2);
});

test('an operand is a float, other is 1', () => {
  expect(sum('1.1', '1')).toBe(1);
});

test('an operand is a float rounded to zero, other is 1', () => {
  expect(sum('0.1', '1')).toBe(1);
});

test('an operand is null, other is 1', () => {
  expect(()=>{sum(null, '1')}).toThrowError('EBADOP');
});