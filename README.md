Kata1
# Description

 - Write a function that takes two integers as string and return the sum as integer
 - If the input operand is not a natural number it will be considered as '0'
 - If an input is null or undefined, it throws an error 'EBAD_OP'

# Installation
 - mpm i

# Usage

 - npm run test
 - practice you TDD